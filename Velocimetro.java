public class Velocimetro {
	
	//ATRIBUTOS
	private double velocidad;
	private double distancia;
	
	//CONSTRUCTOR
	Velocimetro(){
		this.velocidad = 0;
		this.distancia = 0;
	}
	
	//METODOS
	public void set_velocidad(double a){
		
		if(this.velocidad + a > 120){
			System.out.println("Ya se alcanzo la velocidad maxima!");
			this.velocidad = 120;
		}
		
		else {
			this.velocidad = this.velocidad + a;
		}
	}
	
	public double get_velocidad() {
		return this.velocidad;
	}
	
	public void set_distancia(double a) {
		this.distancia = this.distancia + a;
	}
	
	public double get_distancia() {
		return this.distancia;
	}

}
