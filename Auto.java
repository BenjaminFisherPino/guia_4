import java.util.ArrayList;

public class Auto {

	private Ruedas r;
	private Estanque e;
	private Motor m;
	private Velocimetro v;
	private ArrayList<Ruedas> lista;
	private Boolean estado;

	
	//CONSTRUCTOR
	Auto(){
		this.lista = new ArrayList<Ruedas>();
		this.estado = false;
	}
	
	//ESTANQUE//
	public void set_estanque(Estanque e){
		this.e = e;
	}
	
	public Estanque get_estanque() {
		return this.e;
	}
	
	//MOTOR//
	public void set_motor(Motor m){
		this.m = m;
	}
	
	public Motor get_motor() {
		return this.m;
	}
	
	//RUEDAS//
	public void set_ruedas(Ruedas a) {
        this.lista.add(a);
	}
	
	public ArrayList<Ruedas> get_ruedas(){
		return this.lista;
	}
	
	//VELOCIMETRO//
	public void set_velocimetro(Velocimetro a){
		this.v = a;
	}
	
	public Velocimetro get_velocimetro() {
		return this.v;
	}
	
	//METODS PROPIOS DEL AUTO//
	public void encender() {
		this.estado = true;
		e.set_gasolina(e.get_gasolina()/100);
		System.out.println("Se prendio el auto");
	}

	public void apagar() {
		this.estado = false;
	}
	
	public Boolean get_estado() {
		return this.estado;
	}
	
	public void set_velocidad(int a){
		v.set_velocidad(a);
	}	
	
	public void cambio_ruedas(Ruedas a) {
		this.lista.remove(a);
		this.lista.add(new Ruedas());
	}
	
	public void chequeo_ruedas() {
		System.out.println("Usted se baja del auto para hacer un chequeo de" +
		           "las ruedas\n(Deja el auto encendido para que no se apague la calefaccion...)");
        for (int i = 0; i < 4 ; i++) {
        	if (get_ruedas().get(i).get_desgaste() >= 90) {
        		System.out.println("¡OH NO!\nLa rueda " + (i + 1) + " debe ser cambiada");
        		apagar();
        		cambio_ruedas(get_ruedas().get(i));
        		encender();
        	}
	
        	else {
        		if (i == 3) {
        			System.out.println("Las ruedas se encuentran en buenas condiciones");	
        		}
        	}						
        }
	}
}

