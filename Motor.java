import java.util.Random;

public class Motor {
	
	//ATRIBUTOS
	private String cilindrada;
	Random rd = new Random();
	private int a;
	//KILOMETROS POR LITRO
	private double kml;
	
	//CONSTRUCTOR
	Motor(){}
	
	//METODOS
	public void set_cilindrada() {
		a = rd.nextInt(2);
		if (a == 0) {
			this.cilindrada = "1.2";
			this.kml = 20;
		}
		else {
			this.cilindrada = "1.6";
			this.kml = 14;
		}
	}
	
	public String get_cilindrada() {
		return this.cilindrada;
	}
	
	public double get_kml() {
		return this.kml;
	}
}
