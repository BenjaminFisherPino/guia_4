public class Estanque {
	
	//ATRIBUTOS
	private Double gasolina;
	private boolean alarma;
	
	//CONSTRUCTOR
	Estanque(){
		this.gasolina = 100.0;
	}
	
	//METODOS
	public void set_gasolina(Double a){
		if((this.gasolina - a) > 0.0) {
			this.gasolina = this.gasolina - a;	
		}
		else {
			this.gasolina = 0.0;
			System.out.println("Se acabo la gasolina");
		    this.alarma = false;
		}
	}
	
	public Double get_gasolina() {
		return this.gasolina;
	}
	
	public boolean get_alarma() {
		return this.alarma;
	}
}
