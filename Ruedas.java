public class Ruedas {

	//ATRIBUTOS
	private int desgaste;
	private int index;
	
	//CONSTRUCTOR
	Ruedas(){
		this.desgaste = 0;		
	}
	
	//METODOS
	public void set_desgaste(int a) {
		this.desgaste = this.desgaste + a;
	}
	
	public int get_desgaste() {
		return this.desgaste;
	}
	
	public int get_index() {
		return this.index;
	}
}
