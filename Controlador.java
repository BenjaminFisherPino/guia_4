import java.util.Scanner;
import java.util.Random;

public class Controlador {
	
	//ATRIBUTOS
	private int velocidad;
	private int tiempo;

	//CONSTRUCTOR
	Controlador(){
		Auto a = new Auto();		
		Motor m = new Motor();
		Random rd = new Random();
		Estanque e = new Estanque();
		Velocimetro v = new Velocimetro();
		Scanner sc = new Scanner(System.in);
		
		
		a.set_estanque(e);
		a.set_motor(m);
		a.get_motor().set_cilindrada();
		for (int i = 0; i<4; i++) {
			a.set_ruedas(new Ruedas());
		}
		a.set_velocimetro(v);
		a.encender();
		
		while(a.get_estanque().get_gasolina() > 0.0) {
			while(a.get_estado()){
				while (a.get_estanque().get_gasolina() > 0.0) {
				System.out.println("Presione 1 para que el auto se mueva");
				int opcion = sc.nextInt();
				
				if(opcion == 1 && a.get_estanque().get_gasolina() > 0.0) {
					velocidad = rd.nextInt(30) + 1;
					a.set_velocidad(velocidad);
					tiempo = rd.nextInt(3) + 1;
					a.get_velocimetro().set_distancia(a.get_velocimetro().get_velocidad() * tiempo);
					double distancia_temp = a.get_velocimetro().get_velocidad() * tiempo;
					double consumo = ((a.get_velocimetro().get_velocidad() * a.get_motor().get_kml()) / 120);
					double consumo_total = (distancia_temp / consumo);
					a.get_estanque().set_gasolina(consumo_total);
//					System.out.println(consumo_total);
					distancia_temp = 0;
					int desgaste = rd.nextInt(10)  + 1;
					for (int i = 0; i<4; i++) {
						a.get_ruedas().get(i).set_desgaste(desgaste);
						if (rd.nextInt(10) == 0) {
							System.out.println("¡¡¡CUIDADO CON EL CAMINO!!!\nLa rueda " + (i+1) + " por encima de una piedra");
							a.get_ruedas().get(i).set_desgaste(rd.nextInt(20)+1);
						}
					}
						a.chequeo_ruedas();
						
						System.out.println("El auto a recorrido " + a.get_velocimetro().get_distancia() + " Kilometros\n" +
				                   "Se mueve a " + a.get_velocimetro().get_velocidad() + " Km/h\n" + 
						           "Posee " + a.get_estanque().get_gasolina() + " lt de gasolina\n" + 
				                   "Las condiciones de las ruedas son: " + a.get_ruedas().get(0).get_desgaste() + " | " +
				                   a.get_ruedas().get(1).get_desgaste() + " | " + a.get_ruedas().get(2).get_desgaste() + " | " +
				                   a.get_ruedas().get(3).get_desgaste());
				
					}
				}
				a.apagar();
			}		
		}	
	}
}
